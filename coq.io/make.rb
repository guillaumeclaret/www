require "erb"
include ERB::Util

class Template
  def initialize(name)
    @erb = ERB.new(File.read(name, encoding: "UTF-8"))
  end
  
  def result(binding)
    # we remove empty lines
    @erb.result(binding).gsub(/^\s*$\n/, "").chomp
  end
end

module Helpers
  def header(dir, section, subsection)
    Template.new("helpers/header.rhtml").result(binding)
  end
  
  def footer(dir, section)
    Template.new("helpers/footer.rhtml").result(binding)
  end
end

class Page
  include Helpers
  attr_reader :url, :html
  
  def initialize(config, name)
    @url = "#{File.dirname(name)}/#{File.basename(name, ".rhtml")}.html"
    @html = Template.new(name).result(binding)
  end
end

page_names = ["*.rhtml", "language/*.rhtml", "compiler/*.rhtml", "packages/*.rhtml", "about/*.rhtml"]
page_names = (page_names.map {|name| Dir.glob(name)}).flatten
pages = page_names.map {|name| Page.new(@config, name)}
for page in pages do
  File.open("#{page.url}", "w") {|f| f << page.html}
end
puts "#{pages.size} HTML files generated."
